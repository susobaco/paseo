package ca.chancehorizon.paseo

//import kotlinx.android.synthetic.main.fragment_mini_goal.*
//import kotlinx.android.synthetic.main.fragment_mini_goal.view.*
import android.content.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.view.ContextThemeWrapper
import ca.chancehorizon.paseo.databinding.FragmentMiniGoalBinding
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min


class MiniGoalFragment : androidx.fragment.app.Fragment() {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var fragmentMiniGoalBinding: FragmentMiniGoalBinding? = null

    // receiver for step counting service
    private var receiver: BroadcastReceiver? = null

    private lateinit var paseoDBHelper : PaseoDBHelper

    private lateinit var contextThemeWrapper : ContextThemeWrapper

    // flag if a goal is currently in use (set by user when pressing "start" button)
    private var isGoalActive = false

    private var miniGoalSteps = 100
    private var miniGoalStartSteps = 0
    private var miniGoalEndSteps = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // set up the link between this screen and the step counting service
        configureReceiver()
    }



    // set up receiving messages from the step counter service
    private fun configureReceiver() {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateGoalSteps()
            }
        }

        context?.registerReceiver(receiver, filter)
    }



    override fun onDestroy() {

        fragmentMiniGoalBinding = null

        context?.unregisterReceiver(receiver)

        super.onDestroy()
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()
        setStartSteps()
        updateGoalSteps()
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment

        val theTheme = requireContext().theme
        contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view : View = inflater.inflate(R.layout.fragment_mini_goal, container, false)

        val binding = FragmentMiniGoalBinding.bind(view)
        fragmentMiniGoalBinding = binding

        // retrieve from the saved preferences whether text to speech is available on this device
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        val ttsAvailable = paseoPrefs!!.getBoolean("prefTTSAvailable", false)

        // only execute the mini goal feature if text to speech is working on the device
        //  (the only point of the mini goal feature is speaking out the user's step progress)
        if (ttsAvailable) {

            // display the mini goals screen controls if text to speech is not available
            binding.miniGoalsControls.visibility = View.VISIBLE
            binding.goalStepsTitle.text = getString(R.string.mini_goals)

            // respond to the user tapping on the table button
            binding.startButton.setOnClickListener { startGoal() }

            val useDaySteps = paseoPrefs.getBoolean("prefDaySetpsGoal", false)
            binding.dayStepsGoal.isChecked = useDaySteps

            // react to user tapping on the day steps goal switch
            binding.dayStepsGoal.setOnClickListener {
                setStartSteps()
            }

            // react to user tapping on the day steps goal switch
            binding.continueAnnounce.setOnClickListener {
                val edit: SharedPreferences.Editor = paseoPrefs.edit()
                edit.putBoolean("prefContinueAnnounce", binding.continueAnnounce.isChecked)
                edit.apply()
            }

            // set up the number picker that the user uses (not utilizes) to set the goal steps
            val maxSteps = 10000
            val goalStepValuesArray: ArrayList<String> = ArrayList()

            var numSteps = 100

            while (numSteps <= maxSteps) {
                goalStepValuesArray.add(NumberFormat.getIntegerInstance().format(numSteps))
                numSteps = numSteps + 100
            }

            val arrayAdapter: ArrayAdapter<String> = ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_item, goalStepValuesArray)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.goalStepsPicker.setAdapter(arrayAdapter)

            // set the minigoals screen items to visible
            binding.miniGoalsControls.visibility = View.VISIBLE

            // get the most recently set mini goal settings to use as the default values
            // set the default mini goal steps
            miniGoalSteps = paseoPrefs.getInt("prefMiniGoalSteps", 20)
            binding.goalStepsPicker.setSelection(getIndex(binding.goalStepsPicker,
                    NumberFormat.getIntegerInstance().format(miniGoalSteps)))

            // set the default mini goal alert interval
            val miniGoalAlertInterval = paseoPrefs.getInt("prefMiniGoalAlertInterval", 0)
            binding.goalAlertsPicker.setSelection(getIndex(binding.goalAlertsPicker,
                    NumberFormat.getIntegerInstance().format(miniGoalAlertInterval)))
        }
        else
        {
            // hide the mini goals screen controls if text to speech is not available
            binding.miniGoalsControls.visibility = View.GONE
            binding.goalStepsTitle.text = getString(R.string.noTTS)
        }
        return view
    }


    // the the position of a value in a spinner
    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }


    private fun setStartSteps() {

        // retrieve from the saved preferences whether text to speech is available on this device
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        if (fragmentMiniGoalBinding!!.dayStepsGoal.isChecked) {
            // update shared preferences to using day steps as the measure of the minigoal
            val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
            edit.putBoolean("prefDaySetpsGoal", true)
            edit.apply()

            val theGoalSteps = paseoDBHelper.getDaysSteps(SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())

            fragmentMiniGoalBinding!!.goalSteps.text = NumberFormat.getIntegerInstance().format(theGoalSteps)
        }
        else {
            // update shared preferences to using steps starting at zero as the measure of the minigoal
            val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
            edit.putBoolean("prefDaySetpsGoal", false)
            edit.apply()

            fragmentMiniGoalBinding!!.goalSteps.text = "0"
        }
    }



    private fun startGoal() {

        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        isGoalActive = paseoPrefs!!.getBoolean("prefMiniGoalActive", false)

        // toggle running of a mini goal
        isGoalActive = !isGoalActive

        // set up the beginning of this mini goal
        if (isGoalActive) {
            // store the starting steps for this mini goal
            miniGoalStartSteps = paseoDBHelper.readLastEndSteps()

            // update shared preferences to not show first run dialog again
            val edit: SharedPreferences.Editor = paseoPrefs.edit()
            edit.putBoolean("prefMiniGoalActive", true)

            // note that the value saved in prefMiniGoalSteps is the index
            //  of the selected number of steps (not the number of steps)
            val announceInterval: Int
            announceInterval = try {
                stringToInt(fragmentMiniGoalBinding!!.goalAlertsPicker.getSelectedItem() as String)
            } catch(e: NumberFormatException) {
                0
            }
            val miniGoalSteps = stringToInt(fragmentMiniGoalBinding!!.goalStepsPicker.getSelectedItem() as String)

            edit.putInt("prefMiniGoalSteps", miniGoalSteps)
            edit.putInt("prefMiniGoalAlertInterval", announceInterval)
            edit.putInt("prefMiniGoalNextAlert", announceInterval)
            edit.putInt("prefMiniGoalStartSteps", miniGoalStartSteps)
            edit.putInt("prefMiniGoalStartTime", (System.currentTimeMillis() / 1000).toInt())
            edit.apply()

            fragmentMiniGoalBinding!!.goalProgressBar.max = stringToInt(fragmentMiniGoalBinding!!.goalStepsPicker.getSelectedItem() as String)

            fragmentMiniGoalBinding!!.goalProgressBar.secondaryProgress = fragmentMiniGoalBinding!!.goalProgressBar.max

            fragmentMiniGoalBinding!!.goalStepsPicker.isEnabled = false
            fragmentMiniGoalBinding!!.startButton.text = getString(R.string.cancel)
        }
        // close this mini goal
        else {
            // update shared preferences to not show first run dialog again
            val edit: SharedPreferences.Editor = paseoPrefs.edit()
            edit.putBoolean("prefMiniGoalActive", false)
            edit.apply()

            fragmentMiniGoalBinding!!.goalProgressBar.secondaryProgress = 0

            fragmentMiniGoalBinding!!.goalStepsPicker.isEnabled = true

            fragmentMiniGoalBinding!!.startButton.text = getString(R.string.start)
        }
    }



    // strip all non-numeric characters from string and return an integer
    private fun stringToInt(theString: String): Int {
        val re = Regex("[^0-9 ]")
        val digitsOnly = re.replace(theString, "").toInt()
        return digitsOnly
    }



    // fill in the steps
    fun updateGoalSteps() {

        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        isGoalActive = paseoPrefs!!.getBoolean("prefMiniGoalActive", false)
        val useDaySteps = paseoPrefs.getBoolean("prefDaySetpsGoal", false)

        if (isGoalActive) {

            // disable the number pick for the mini goal steps (to prevent changes while goal is running
            fragmentMiniGoalBinding!!.goalStepsPicker.isEnabled = false
            fragmentMiniGoalBinding!!.startButton.text = getString(R.string.cancel)

            // get the mini goal settings
            miniGoalSteps = paseoPrefs.getInt("prefMiniGoalSteps", 20)
            NumberFormat.getIntegerInstance().format(miniGoalSteps)

            // make sure the picker is showing the current mini goal steps
            fragmentMiniGoalBinding!!.goalStepsPicker.setSelection(getIndex(fragmentMiniGoalBinding!!.goalStepsPicker,
                    NumberFormat.getIntegerInstance().format(miniGoalSteps)))

            // get the mini goal settings
            val miniGoalAlertInterval = paseoPrefs.getInt("prefMiniGoalAlertInterval", 0)
            // make sure the picker is showing the current mini goal steps
            fragmentMiniGoalBinding!!.goalAlertsPicker.setSelection(getIndex(fragmentMiniGoalBinding!!.goalAlertsPicker,
                    NumberFormat.getIntegerInstance().format(miniGoalAlertInterval)))

            miniGoalStartSteps = paseoPrefs.getInt("prefMiniGoalStartSteps", 0)

            val goalStepCount: Int

            // default to the steps starting at zero (or use the day's set count, if user has set that option)
            if (!useDaySteps) {
                goalStepCount = miniGoalEndSteps - miniGoalStartSteps
                // update endsteps
                miniGoalEndSteps = paseoDBHelper.readLastEndSteps()
            }
            // or get the current day's steps
            else {
                goalStepCount = paseoDBHelper.getDaysSteps(SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            }

            // sometimes, owing to the timing of starting the goal, the value of stepCount is < 0
            //  when that happens, do not display
            if (goalStepCount > 0) {
                // display the goal steps
                fragmentMiniGoalBinding!!.goalSteps.text = NumberFormat.getIntegerInstance().format(goalStepCount)
            }

            // advance the progressbar to the actual number of steps taken so far for this goal
            fragmentMiniGoalBinding!!.goalProgressBar.progress = min(goalStepCount, miniGoalSteps)
            fragmentMiniGoalBinding!!.goalProgressBar.max = miniGoalSteps

            // check if mini goal has been achieved
            if (goalStepCount >= miniGoalSteps) {
                // reset the interface
                startGoal()
                fragmentMiniGoalBinding!!.startButton.text = getString(R.string.start)
            }
        }
    }
}