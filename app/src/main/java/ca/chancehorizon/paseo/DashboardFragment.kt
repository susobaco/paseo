package ca.chancehorizon.paseo



import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ca.chancehorizon.paseo.databinding.FragmentDashboardBinding
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt



class DashboardFragment : androidx.fragment.app.Fragment() {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var fragmentDashboardBinding: FragmentDashboardBinding? = null

    var running = false
    var startSteps = 0

    val daysInMonth = getDaysInMonth(-1)
    val daysInYear = if (isLeapYear(Calendar.getInstance().get(Calendar.YEAR))) 366 else 365

    // default values for target steps (overridden later from shared preferences)
    var targetSteps = 10000
    var targetWeekSteps = 7000
    // typically there are thirty days in a month.  This will be overridden later based on actual days in the month
    var targetMonthSteps = 30000
    var targetYearSteps = 3650000

    // default the starting day of week to Monday (overridden later from saved settings)
    var weekStart = 2

    var lastStepDate = 0

    var receiver: BroadcastReceiver? = null

    lateinit var paseoDBHelper : PaseoDBHelper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // get the date of the last record from the steps table in the database
        lastStepDate = paseoDBHelper.readLastStepsDate()

        // get the start steps of the last record from the steps table in the database
        startSteps = paseoDBHelper.readLastStartSteps()

        startSteps = startSteps + 0

        updateDashboard()

        configureReceiver()

        running = true

    }



    // set up receiving messages from the step counter service
    private fun configureReceiver()
    {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateDashboard()
            }
        }

        context?.registerReceiver(receiver, filter)
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        // Inflate the layout for this fragment
        val view : View = inflater.inflate(R.layout.fragment_dashboard, container, false)

        val binding = FragmentDashboardBinding.bind(view)
        fragmentDashboardBinding = binding

        // respond to the user tapping on the dashboard legend
        //  (essentially making the legend into a button)
        binding.legendLayout.setOnClickListener { _ ->
            // show the dashboard legend detailed descriptions dialog sheet
            val bottomSheet = DashboardLegendFragment()
            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        targetSteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()

        targetWeekSteps = targetSteps * 7
        targetMonthSteps = targetSteps * daysInMonth
        targetYearSteps = targetSteps * daysInYear

        // default to Monday as first day of week
        weekStart = paseoPrefs.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

        // respond to the user tapping on the day steps progress circles
        //  (essentially making the day steps progress circles a button)
        binding.dayCard.setOnClickListener { _ ->
            val theSteps = paseoDBHelper.getDaysSteps(SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("days", targetSteps)
            val averageSteps = paseoDBHelper.getAverageSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val maxStepsArray = paseoDBHelper.getMaxSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val (maxSteps, unused) = maxStepsArray[0]
            val minSteps = paseoDBHelper.getMinSteps("days", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val theHour = SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()

            // fill in all the step details in the details bottom sheet
            val bottomSheet = DashboardStepsDetailsFragment()

            bottomSheet.timeUnit = getString(R.string.day)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for) + SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetSteps * max(theHour, 1) / 24
            bottomSheet.projectedSteps = theSteps * 24 / max(theHour, 1)
            bottomSheet.targetSteps = targetSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the week steps progress circles
        //  (essentially making the week steps progress circles a button)
        binding.weekCard.setOnClickListener { _ ->

            val theSteps = paseoDBHelper.getSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(),0, weekStart)
            val onTarget = paseoDBHelper.getOnTarget("weeks", targetWeekSteps, weekStart)
            val maxStepsArray = paseoDBHelper.getMaxSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), 0, weekStart)
            val (maxSteps, unused) = maxStepsArray[0]
            val minSteps = paseoDBHelper.getMinSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), 0, weekStart)
            val averageSteps = paseoDBHelper.getAverageSteps("weeks", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt(), 0, weekStart)
            val theDayOfWeek = getDayOfWeekNumber(Date(), weekStart)
            val theHour = SimpleDateFormat("HH", Locale.getDefault()).format(Date()).toInt()

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = getString(R.string.week)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for_week_of) + SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(getFirstDayInWeek(weekStart))
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetWeekSteps * ((theDayOfWeek - 1) * 24 + theHour) / (7 * 24)
            bottomSheet.projectedSteps = theSteps * (7 * 24) / ((theDayOfWeek - 1) * 24 + theHour)
            bottomSheet.targetSteps = targetWeekSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the month steps progress circles
        //  (essentially making the month steps progress circles a button)
        binding.monthCard.setOnClickListener { _ ->

            val theSteps = paseoDBHelper.getSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("months", targetMonthSteps)
            val maxStepsArray = paseoDBHelper.getMaxSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val (maxSteps, unused) = maxStepsArray[0]
            val minSteps = paseoDBHelper.getMinSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("months", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val theDayOfMonth = SimpleDateFormat("d", Locale.getDefault()).format(Date()).toInt()

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = getString(R.string.month)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for) + SimpleDateFormat("MMMM", Locale.getDefault()).format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetMonthSteps * theDayOfMonth / daysInMonth
            bottomSheet.projectedSteps = theSteps * daysInMonth / theDayOfMonth
            bottomSheet.targetSteps = targetMonthSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        // respond to the user tapping on the year steps progress circles
        //  (essentially making the year steps progress circles a button)
        binding.yearCard.setOnClickListener { _ ->

            val theSteps = paseoDBHelper.getSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val onTarget = paseoDBHelper.getOnTarget("years", targetYearSteps)
            val maxStepsArray = paseoDBHelper.getMaxSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val (maxSteps, unused) = maxStepsArray[0]
            val minSteps = paseoDBHelper.getMinSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val averageSteps = paseoDBHelper.getAverageSteps("years", SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
            val calendar = Calendar.getInstance()
            calendar.time = Date()
            val daysInYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
            val theDayOfYear = calendar.get(Calendar.DAY_OF_YEAR) // the day of the year in numerical format

            // show the dashboard steps detailed descriptions dialog sheet
            val bottomSheet = DashboardStepsDetailsFragment()
            bottomSheet.timeUnit = getString(R.string.year)

            bottomSheet.timeUnitSummaryTitle = getString(R.string.summary_for) + SimpleDateFormat("yyyy", Locale.getDefault()).format(Date())
            bottomSheet.steps = theSteps
            bottomSheet.expectedSteps = targetYearSteps * theDayOfYear / daysInYear
            bottomSheet.projectedSteps = theSteps * daysInYear / theDayOfYear
            bottomSheet.targetSteps = targetYearSteps

            bottomSheet.timesOnTarget = onTarget
            bottomSheet.averageSteps = averageSteps
            bottomSheet.maximumSteps = maxSteps
            bottomSheet.minimumSteps = minSteps

            bottomSheet.show(childFragmentManager, "BottomSheet")
        }

        return view
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()

        updateDashboard()
    }



    override fun onDestroy() {
        // Do not store the binding instance in a field, if not required.
        fragmentDashboardBinding = null

        context?.unregisterReceiver(receiver)
        super.onDestroy()
    }



    // update the data displayed in the dashboard
    fun updateDashboard()
    {
        if (running)
        {
            // get the application settings
            val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
            targetSteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()

            // default to Monday as first day of week
            weekStart = paseoPrefs.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

            var dateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault()) // looks like "19891225"
            val date = dateFormat.format(Date()).toInt()
            dateFormat = SimpleDateFormat("HH", Locale.getDefault())
            val hour = dateFormat.format(Date()).toInt()

            dateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) // looks like "Dec 25, 1989"
            fragmentDashboardBinding!!.dateValue.text = dateFormat.format(Date())

            // get today's steps from the steps table in the database
            var theSteps : Int = paseoDBHelper.getDaysSteps(date)

            // display today's steps on the main screen
            fragmentDashboardBinding!!.stepsValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            fragmentDashboardBinding!!.stepsProgressBar.max = targetSteps
            fragmentDashboardBinding!!.stepsProgressBar.progress = theSteps
            fragmentDashboardBinding!!.stepsProgressBar.secondaryProgress = targetSteps
            var theText = (theSteps.toDouble() / targetSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetSteps) + ")"
            fragmentDashboardBinding!!.percentTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            fragmentDashboardBinding!!.expectedStepsProgressBar.max = 24
            fragmentDashboardBinding!!.expectedStepsProgressBar.progress = hour

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            fragmentDashboardBinding!!.projectedStepsProgressBar.max = targetSteps
            try {
                fragmentDashboardBinding!!.projectedStepsProgressBar.progress = min(theSteps * 24 / max(hour, 1), targetSteps)
            }
            catch (e: SQLiteException){

            }

            if (weekStart == 2) {
                fragmentDashboardBinding!!.dateWeekValue.text = getString(R.string.week_title)
            }
            else {
                fragmentDashboardBinding!!.dateWeekValue.text = getString(R.string.week_title2)
            }

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("weeks", date, 0 ,weekStart)

            // display today's steps on the main screen
            fragmentDashboardBinding!!.stepsWeekValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            fragmentDashboardBinding!!.stepsWeekProgressBar.max = targetWeekSteps
            fragmentDashboardBinding!!.stepsWeekProgressBar.progress = theSteps
            fragmentDashboardBinding!!.stepsWeekProgressBar.secondaryProgress = targetWeekSteps
            theText = (theSteps.toDouble() / targetWeekSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetWeekSteps) + ")"
            fragmentDashboardBinding!!.percentWeekTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            val theDayOfWeek = getDayOfWeekNumber(Date(), weekStart)

            fragmentDashboardBinding!!.expectedWeekStepsProgressBar.max = 7 * 24
            fragmentDashboardBinding!!.expectedWeekStepsProgressBar.progress = (theDayOfWeek - 1) * 24 + hour

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            fragmentDashboardBinding!!.projectedWeekStepsProgressBar.max = targetWeekSteps
            try {
                fragmentDashboardBinding!!.projectedWeekStepsProgressBar.progress = min(theSteps * 7 / theDayOfWeek, targetWeekSteps)
            }
            catch (e: SQLiteException){
            }


            dateFormat = SimpleDateFormat("MMMM", Locale.getDefault()) // looks like "Dec 25, 1989

            theText = dateFormat.format(Date())
            fragmentDashboardBinding!!.dateMonthValue.text = theText

            // get this month's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("months", date)

            // display this month's steps on the main screen
            fragmentDashboardBinding!!.stepsMonthValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            fragmentDashboardBinding!!.stepsMonthProgressBar.max = targetMonthSteps
            fragmentDashboardBinding!!.stepsMonthProgressBar.progress = theSteps
            fragmentDashboardBinding!!.stepsMonthProgressBar.secondaryProgress = targetMonthSteps
            theText = (theSteps.toDouble() / targetMonthSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetMonthSteps) + ")"
            fragmentDashboardBinding!!.percentMonthTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            dateFormat = SimpleDateFormat("d", Locale.getDefault())
            val dayOfMonth = dateFormat.format(Date()).toInt()
            fragmentDashboardBinding!!.expectedMonthStepsProgressBar.max = daysInMonth
            fragmentDashboardBinding!!.expectedMonthStepsProgressBar.progress = dayOfMonth

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            fragmentDashboardBinding!!.projectedMonthStepsProgressBar.max = targetMonthSteps
            try {
                fragmentDashboardBinding!!.projectedMonthStepsProgressBar.progress = min(theSteps * daysInMonth / dayOfMonth, targetMonthSteps)
            }
            catch (e: SQLiteException){
            }


            dateFormat = SimpleDateFormat("yyyy", Locale.getDefault()) // looks like "1989"
            fragmentDashboardBinding!!.dateYearValue.text = dateFormat.format(Date())

            // get this week's steps from the steps table in the database
            theSteps = paseoDBHelper.getSteps("years", date)

            // display today's steps on the main screen
            fragmentDashboardBinding!!.stepsYearValue.text = NumberFormat.getIntegerInstance().format(theSteps)
            fragmentDashboardBinding!!.stepsYearProgressBar.max = targetYearSteps
            fragmentDashboardBinding!!.stepsYearProgressBar.progress = theSteps
            fragmentDashboardBinding!!.stepsYearProgressBar.secondaryProgress = targetYearSteps
            theText = (theSteps.toDouble() / targetYearSteps.toDouble() * 100.0).roundToInt().toString() + getString(R.string.percentage_target) +
                    NumberFormat.getIntegerInstance().format(targetYearSteps) + ")"
            fragmentDashboardBinding!!.percentYearTargetValue.text = theText

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            dateFormat = SimpleDateFormat("D", Locale.getDefault())
            val dayOfYear = dateFormat.format(Date()).toInt()
            fragmentDashboardBinding!!.expectedYearStepsProgressBar.max = daysInYear
            fragmentDashboardBinding!!.expectedYearStepsProgressBar.progress = dayOfYear

            // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
            //  the daily target steps
            fragmentDashboardBinding!!.projectedYearStepsProgressBar.max = targetYearSteps
            try {
                fragmentDashboardBinding!!.projectedYearStepsProgressBar.progress = min(theSteps * daysInYear / dayOfYear, targetYearSteps)
            }
            catch (e: SQLiteException){
            }
        }
    }
}


