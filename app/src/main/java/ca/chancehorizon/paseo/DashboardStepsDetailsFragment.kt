package ca.chancehorizon.paseo


import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import ca.chancehorizon.paseo.databinding.DashboardStepsDetailsBottomsheetBinding

//import kotlinx.android.synthetic.main.dashboard_steps_details_bottomsheet.*
import java.text.NumberFormat


class DashboardStepsDetailsFragment : BottomSheetDialogFragment(), View.OnClickListener {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var dashboardStepsDetailsBinding: DashboardStepsDetailsBottomsheetBinding? = null


    var timeUnit: String = "day"
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var timeUnitSummaryTitle: String = "Summary for..."
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var steps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var expectedSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var projectedSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var targetSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var timesOnTarget: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var averageSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var maximumSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }

    var minimumSteps: Int = 0
        get() = field        // getter
        set(value) {         // setter
            field = value
        }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val theTheme = requireContext().getTheme()
        val contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view = inflater.cloneInContext(contextThemeWrapper).inflate(R.layout.dashboard_steps_details_bottomsheet, container, false)

        val binding = DashboardStepsDetailsBottomsheetBinding.bind(view)
        dashboardStepsDetailsBinding = binding

        return view

    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // respond to the user tapping anywhere in the dialog
        dashboardStepsDetailsBinding!!.legendLayout.setOnClickListener(this)

        // fill in the details for this timeUnit
        updateDetail()
    }



    override fun onDestroy() {
        // Do not store the binding instance in a field, if not required.
        dashboardStepsDetailsBinding = null

        super.onDestroy()
    }



    fun updateDetail() {

        dashboardStepsDetailsBinding!!.detailTimeUnitSummaryTitle.text = timeUnitSummaryTitle
        dashboardStepsDetailsBinding!!.detailProjectedSteps.text = timeUnit
        dashboardStepsDetailsBinding!!.detailSteps.text = NumberFormat.getIntegerInstance().format(steps)
        dashboardStepsDetailsBinding!!.detailExpectedSteps.text = NumberFormat.getIntegerInstance().format(expectedSteps)
        dashboardStepsDetailsBinding!!.detailProjectedSteps.text = NumberFormat.getIntegerInstance().format(projectedSteps)
        dashboardStepsDetailsBinding!!.detailTargetSteps.text = NumberFormat.getIntegerInstance().format(targetSteps)

        val summaryTitle = getString(R.string.summary_all) + (timeUnit.capitalized()).plus("s")
        dashboardStepsDetailsBinding!!.detailAllTimeSummaryTitle.text = summaryTitle
        dashboardStepsDetailsBinding!!.timesOnTargetLabel.text = " ".plus(timeUnit.capitalized()).plus(getString(R.string.on_target))
        dashboardStepsDetailsBinding!!.detailTimesOnTarget.text = timesOnTarget.toString()
        dashboardStepsDetailsBinding!!.detailAverageSteps.text = NumberFormat.getIntegerInstance().format(averageSteps)
        dashboardStepsDetailsBinding!!.detailMaximumSteps.text = NumberFormat.getIntegerInstance().format(maximumSteps)
        dashboardStepsDetailsBinding!!.detailMinimumSteps.text = NumberFormat.getIntegerInstance().format(minimumSteps)
    }



    // respond to user taps
    override fun onClick(view: View) {

        // close the dialog
        dismiss()
    }
}