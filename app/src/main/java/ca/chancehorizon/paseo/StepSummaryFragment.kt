package ca.chancehorizon.paseo


import android.content.*
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.content.ContextCompat
import ca.chancehorizon.paseo.databinding.FragmentStepSummaryBinding
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.Legend.LegendForm
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.material.snackbar.Snackbar
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


class StepSummaryFragment : androidx.fragment.app.Fragment(), OnChartValueSelectedListener {

    // Scoped to the lifecycle of the fragment's view (between onCreateView and onDestroyView)
    private var fragmentStepSummaryBinding: FragmentStepSummaryBinding? = null

    // the timeUnit variable allows for the same class to be used for
    //  fragments (screens) showing a summary for different time units
    var timeUnit: String = "???"
        get() = field        // getter
        set(value) {         // setterfirst run dialog again
            field = value
        }

    var running = false

    var targetDaySteps = 10000
    // target steps for months and years are calculated later based on actual number of days in
    //  the month (28 to 31) and year (365 or 366)
    //  all the calendar/time calculation would be so much simpler using metric time

    // default to showing the graph instead of the table (will be overriden later from user setting - store in prefs)
    var isTableShown = false

    // receiver for step counting service
    var receiver: BroadcastReceiver? = null

    lateinit var paseoDBHelper : PaseoDBHelper

    lateinit var contextThemeWrapper : ContextThemeWrapper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(requireActivity())

        // fill the screen with the latest steps data
        updateSummary()

        // set up the link between this screen and the step counting service
        configureReceiver()

        running = true
    }



    // set up receiving messages from the step counter service
    private fun configureReceiver() {
        val filter = IntentFilter()
        filter.addAction("ca.chancehorizon.paseo.action")
        filter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                updateSummary()
            }
        }

        context?.registerReceiver(receiver, filter)
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment

        val theTheme = requireContext().theme
        contextThemeWrapper = ContextThemeWrapper(activity, theTheme)

        val view : View = inflater.inflate(R.layout.fragment_step_summary, container, false)

        val binding = FragmentStepSummaryBinding.bind(view)
        fragmentStepSummaryBinding = binding

        // respond to the user tapping on the table button
        binding.editButton.setOnClickListener {
            editSteps()
        }

        // respond to the user tapping on the table button
        binding.tableButton.setOnClickListener {
            showTable(true)
        }

        // respond to the user tapping on the graph button
        binding.graphButton.setOnClickListener {
            showTable(false)
        }

        return view
    }



    override fun onDestroy() {

        fragmentStepSummaryBinding = null

        context?.unregisterReceiver(receiver)

        super.onDestroy()

    }



    // needed for reacting to tapping on barchart (this is when tapping outside of the chart area)
    override fun onNothingSelected() {

    }



    // respond to user tapping on bar in barchart - show a "snackbar" with the number of steps for
    //  the time period of the bar that was tapped
    override fun onValueSelected(theBar: Entry?, h: Highlight?) {
        val snack = Snackbar.make(requireView(), NumberFormat.getIntegerInstance().format(theBar?.y) + " steps", Snackbar.LENGTH_SHORT)
        snack.view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.grey_900))
        val textView = snack.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(requireContext(), R.color.grey_50))

        snack.show()
    }



    // re-update the screen when the user returns to it from another screen
    override fun onResume() {
        super.onResume()
        updateSummary()

        // let the main activity of Paseo know which help screen to show for the step summary fragment
        (activity as MainActivity).theScreen = timeUnit

        // let the main activity of Paseo know which screen to return to when user presses the back button
        (activity as MainActivity).restoreScreen = "dashboard"
    }



    // display the edit steps screen for the user selected date/time
    fun editSteps(theDate: Int = 0): Boolean {

        val fragment: androidx.fragment.app.Fragment?

        fragment = EditStepsFragment()
        fragment.timeUnit = timeUnit

        // if there is a date set, send it to the edit
        if (theDate != 0) {
            fragment.theDate = theDate
        }

        requireActivity()
                .supportFragmentManager
                .beginTransaction()
                .replace(this.id, fragment)
                .commitNow()

        return true
    }



    // toggle between showing the steps table and showing the steps graph
    fun showTable(showTheTable : Boolean) {

        // show the table and hide the graph (also reformat the buttons accordingly)
        if (showTheTable) {
            if (fragmentStepSummaryBinding!!.stepsListHeader.visibility == View.GONE) {
                fragmentStepSummaryBinding!!.stepsListHeader.visibility = View.VISIBLE
                fragmentStepSummaryBinding!!.stepsListView.visibility = View.VISIBLE
            }
            fragmentStepSummaryBinding!!.stepsGraph.visibility = View.GONE

            fragmentStepSummaryBinding!!.tableButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorPrimary))
            fragmentStepSummaryBinding!!.tableButton.setBackgroundResource(R.drawable.bottom_border_on)
            fragmentStepSummaryBinding!!.graphButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorSecondary))
            fragmentStepSummaryBinding!!.graphButton.setBackgroundResource(R.drawable.bottom_border)
        }
        // hide the table and show the graph (also reformat the buttons accordingly)
        else if (!showTheTable && fragmentStepSummaryBinding!!.stepsListHeader.visibility == View.VISIBLE) {
            if (fragmentStepSummaryBinding!!.stepsListHeader.visibility == View.VISIBLE) {
                fragmentStepSummaryBinding!!.stepsListHeader.visibility = View.GONE
                fragmentStepSummaryBinding!!.stepsListView.visibility = View.GONE
            }
            fragmentStepSummaryBinding!!.stepsGraph.visibility = View.VISIBLE

            fragmentStepSummaryBinding!!.tableButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorSecondary))
            fragmentStepSummaryBinding!!.tableButton.setBackgroundResource(R.drawable.bottom_border)
            fragmentStepSummaryBinding!!.graphButton.setTextColor(resolveColorAttr(requireView().context, android.R.attr.textColorPrimary))
            fragmentStepSummaryBinding!!.graphButton.setBackgroundResource(R.drawable.bottom_border_on)
        }

        // update shared preferences to not show the table or graph
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
        val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
        edit.putBoolean("prefShowTableInSummary", showTheTable)
        edit.apply()

    }



    // fill in the steps data on the screen based on the time unit selected by the user (from the menu)
    fun updateSummary() {

        if (running && timeUnit != "miniGoals")
        {
            var theDateFormat = SimpleDateFormat("yyyyMMdd", Locale.getDefault()) // looks like "19891225"
            val date = theDateFormat.format(Date()).toInt()
            theDateFormat = SimpleDateFormat("mm", Locale.getDefault()) // looks like 00, 01... 58, 59
            val minute = max(theDateFormat.format(Date()).toInt(), 1) // value needs to be at least 1 in order to prevent divide by zero error
            theDateFormat = SimpleDateFormat("HH", Locale.getDefault()) // looks like 00, 01... 23, 24
            val hour = max(theDateFormat.format(Date()).toInt(), 1) // value needs to be at least 1 in order to prevent divide by zero error

            // get the application settings (save messages etc)
            val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)
            targetDaySteps = paseoPrefs!!.getFloat("prefDailyStepsTarget", 10000F).toInt()
            // default to Monday as first day of week
            val weekStart = paseoPrefs.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())!!.toInt()

            val dayOfWeek = getDayOfWeekNumber(Date(), weekStart)

            val calendar = Calendar.getInstance()

            calendar.time = Date()
            val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) // the day of the month in numerical format
            val dayOfYear = calendar.get(Calendar.DAY_OF_YEAR) // the day of the year in numerical format

            var theSteps = 0
            var projectedSteps = 0

            var targetSteps = targetDaySteps // to be overriden later for differnt time units
            val stepsArray: ArrayList<Array<Int>>
            var projectedProgress = 0
            var expectedProgress = 0

            var datePrefix = " "
            var theDate = Date()

            // if the time unit was not set when calling this fragment, read it from share preferences
            if (timeUnit == "???") {
                timeUnit = paseoPrefs.getString("prefLastTimeUnit", "days")!!
            }
            // set basic parameters based on time unit summary being assembled.
            when (timeUnit) {
                "hours" -> {
                    targetSteps = targetDaySteps / 10
                    theDateFormat = SimpleDateFormat("MMM dd, yyyy, HH:00", Locale.getDefault()) // looks like "Dec 25, 1989  13:00"
                    theSteps = paseoDBHelper.getSteps("hours", hour, date)
                    projectedSteps = theSteps * 60 / minute
                    projectedProgress = min(100 * theSteps * 60 / minute / targetSteps, 100)
                    expectedProgress = 100 * minute / 60
                }
                "days" -> {
                    targetSteps = targetDaySteps
                    theDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) // looks like "Dec 25, 1989"
                    theSteps = paseoDBHelper.getDaysSteps(date)
                    projectedSteps = theSteps * 24 / hour
                    projectedProgress = min(100 * theSteps * 24 / hour / targetSteps, 100)
                    expectedProgress = 100 * hour / 24
                }
                "weeks" -> {
                    targetSteps = targetDaySteps * 7
                    theDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) // looks like "Dec 25, 1989"
                    theSteps = paseoDBHelper.getSteps("weeks", date, 0, weekStart)
                    projectedSteps = theSteps * 7 / dayOfWeek
                    projectedProgress = min(100 * theSteps * 7 / dayOfWeek / targetSteps, 100)
                    expectedProgress = 100 * dayOfWeek / 7
                    datePrefix = "Week of "
                    theDate = getFirstDayInWeek(weekStart)
                }
                "months" -> {
                    theDateFormat = SimpleDateFormat("MMMM", Locale.getDefault()) // looks like "December"
                    theSteps = paseoDBHelper.getSteps("months", date)
                    val daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
                    targetSteps = targetDaySteps * daysInMonth
                    projectedSteps = theSteps * daysInMonth / dayOfMonth
                    projectedProgress = min(100 * theSteps * daysInMonth / dayOfMonth / targetSteps, 100)
                    expectedProgress = 100 * dayOfMonth / daysInMonth
                }
                "years" -> {
                    theDateFormat = SimpleDateFormat("yyyy", Locale.getDefault()) // looks like "1989"
                    theSteps = paseoDBHelper.getSteps("years", date)
                    val daysInYear = calendar.getActualMaximum(Calendar.DAY_OF_YEAR)
                    targetSteps = targetDaySteps * daysInYear
                    projectedSteps = theSteps * (daysInYear / dayOfYear)
                    projectedProgress = min(100 * theSteps * (daysInYear / dayOfYear) / targetSteps, 100)
                    expectedProgress = 100 * dayOfYear / daysInYear
                }
            }
            var theText = datePrefix + theDateFormat.format(theDate)
            fragmentStepSummaryBinding!!.dateValue.text = theText

            // get steps for every time unit from the steps table in the database
            stepsArray = paseoDBHelper.getStepsByTimeUnit(date, timeUnit, true, weekStart)

            // only fill in the steps table and steps graph if steps have been recorded
            //  (otherwise get an error, when trying to display graph the first time Paseo is run)
            if (stepsArray.size > 0 ) {

                createStepsTable(stepsArray)

                // display today's steps on the main screen
                theText = NumberFormat.getIntegerInstance().format(theSteps)
                fragmentStepSummaryBinding!!.stepsValue.text = theText
                fragmentStepSummaryBinding!!.stepsProgressBar.max = targetSteps // need to set/save target steps in preferences
                fragmentStepSummaryBinding!!.stepsProgressBar.progress = theSteps
                fragmentStepSummaryBinding!!.stepsProgressBar.secondaryProgress = targetSteps
                theText = (theSteps.toDouble() / targetSteps.toDouble() * 100.0).roundToInt().toString() + "% of target (" +
                        NumberFormat.getIntegerInstance().format(targetSteps) + ")"
                fragmentStepSummaryBinding!!.percentTargetValue.text = theText

                // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
                //  the daily target steps
                fragmentStepSummaryBinding!!.expectedStepsProgressBar.progress = expectedProgress

                // display the "expected" steps progress (expected is the number of steps needed by this hour to achieve
                //  the daily target steps
                fragmentStepSummaryBinding!!.projectedStepsProgressBar.max = 100
                try {
                    fragmentStepSummaryBinding!!.projectedStepsProgressBar.progress = projectedProgress
                }
                catch (e: SQLiteException) {
                    fragmentStepSummaryBinding!!.projectedStepsProgressBar.progress = 0
                }

                // get this hour's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("hours", hour, date)
                // display this hour's steps on the main screen
                fragmentStepSummaryBinding!!.hourStepsLabel.text = (getString(R.string.steps_this_hour))
                fragmentStepSummaryBinding!!.hourStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this week's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("days", date)
                // display this hour's steps on the main screen
                fragmentStepSummaryBinding!!.dayStepsLabel.text = (getString(R.string.steps_this_day))
                fragmentStepSummaryBinding!!.dayStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this week's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("weeks", date, 0, weekStart)
                // display this hour's steps on the main screen
                fragmentStepSummaryBinding!!.weekStepsLabel.text = (getString(R.string.steps_this_week))
                fragmentStepSummaryBinding!!.weekStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this month's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("months", date)
                // display this month's steps on the main screen
                fragmentStepSummaryBinding!!.monthStepsLabel.text = (getString(R.string.steps_this_month) )
                fragmentStepSummaryBinding!!.monthStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                // get this month's steps from the steps table in the database
                theSteps = paseoDBHelper.getSteps("years", date)
                // display this month's steps on the main screen
                fragmentStepSummaryBinding!!.yearStepsLabel.text = (getString(R.string.steps_this_year))
                fragmentStepSummaryBinding!!.yearStepsValue.text = (NumberFormat.getIntegerInstance().format(theSteps))

                createStepsGraph(stepsArray, projectedSteps, targetSteps)

                // show the table instead of the graph based on most recent user selection (from prefs)
                isTableShown = paseoPrefs.getBoolean("prefShowTableInSummary", false)
                showTable(isTableShown)

            }
        }
    }



    fun createStepsTable(stepsArray: ArrayList<Array<Int>>){
        lateinit var listView: ListView
        val adapter: StepsTableAdapter?

        listView = fragmentStepSummaryBinding!!.stepsListView

        // reverse the array so that the steps table displays in reverse chronological order
        //  most recent first
        val tempStepsArray = ArrayList(stepsArray)
        tempStepsArray.reverse()

        // set up the array adapter so that the table displays quickly when there are lots of rows
        // (array adapter load rows as the user scroll rather than having all rows loaded at once)
        adapter = StepsTableAdapter(requireContext(), tempStepsArray, timeUnit)
        listView.adapter = adapter

        // show a steps editing screen when the user long presses on a steps table row
        listView.setOnItemLongClickListener { arg0, arg1, pos, id ->
            editSteps(arg1.tag as Int)
            true
        }

        setListViewHeightBasedOnChildren(listView as ListView?)

        // put the correct title in the date/time unit column header
        if (timeUnit == "hours") {
            fragmentStepSummaryBinding!!.rowDateTimeHeader.text = getString(R.string.hour_cap)
        }
        else {
            fragmentStepSummaryBinding!!.rowDateTimeHeader.text = getString(R.string.date_cap)
        }
    }



    fun setListViewHeightBasedOnChildren(myListView: ListView?) {
        val adapter = myListView!!.adapter
        if (myListView != null) {
            var totalHeight = 0

            val numVisibleRows = min(adapter.count, 10)

            for (i in 0 until numVisibleRows) {
                val item = adapter.getView(i, null, myListView)
                item.measure(0, 0)
                totalHeight += item.measuredHeight
            }
            val params = myListView.layoutParams
            params.height = totalHeight + myListView.dividerHeight * numVisibleRows
            myListView.layoutParams = params
        }
    }



    fun createStepsGraph (stepsArray: ArrayList<Array<Int>>, projectedSteps: Int, targetSteps: Int) {

        // get the application settings
        val paseoPrefs = context?.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // default to Monday as first day of week
        val weekStart = paseoPrefs?.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())?.toInt()

        // begin setting up the graph
        fragmentStepSummaryBinding!!.stepsGraph.setOnChartValueSelectedListener(this)

        fragmentStepSummaryBinding!!.stepsGraph.setDrawBarShadow(false)
        fragmentStepSummaryBinding!!.stepsGraph.setDrawValueAboveBar(false)
        fragmentStepSummaryBinding!!.stepsGraph.setDrawValueAboveBar(false)

        fragmentStepSummaryBinding!!.stepsGraph.description.isEnabled = false

        // if more than 366 entries are displayed in the chart, no values will be
        // drawn
        fragmentStepSummaryBinding!!.stepsGraph.setMaxVisibleValueCount(366)

        // scaling done on x- and y-axis separately
        fragmentStepSummaryBinding!!.stepsGraph.setPinchZoom(false)

        fragmentStepSummaryBinding!!.stepsGraph.setDrawGridBackground(false)

        val xAxis = fragmentStepSummaryBinding!!.stepsGraph.xAxis
        xAxis.position = XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f // show an x axis label for a maximum of every bar
        xAxis.labelCount = 7 // show a maximum of 7 x axis labels (skipping labels if more than 7 bars)
        xAxis.setCenterAxisLabels(false)
        xAxis.setDrawGridLines(false)
        xAxis.labelRotationAngle = -45f

        val leftAxis = fragmentStepSummaryBinding!!.stepsGraph.axisLeft
        leftAxis.setLabelCount(8, false)
        leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.spaceTop = 15f
        leftAxis.axisMinimum = 0f

        val rightAxis = fragmentStepSummaryBinding!!.stepsGraph.axisRight
        rightAxis.setDrawGridLines(false)
        rightAxis.setLabelCount(8, false)
        rightAxis.spaceTop = 15f
        rightAxis.axisMinimum = 0f

        val graphLegend = fragmentStepSummaryBinding!!.stepsGraph.legend
        graphLegend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        graphLegend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        graphLegend.orientation = Legend.LegendOrientation.HORIZONTAL
        graphLegend.setDrawInside(false)
        graphLegend.form = LegendForm.SQUARE
        graphLegend.formSize = 9f
        graphLegend.textSize = 11f
        graphLegend.xEntrySpace = 4f

        val entries = ArrayList<BarEntry>()

        var maxBarsToShow = 10
        var maxXVal = 0
        var newXVal = 0
        // save the first time/date value so that the graph and x axis labels start at the same place and are aligned
        var minXVal = stepsArray[0][0]
        val xAxisLabels = ArrayList<String>()

        // calendar instance to be used to create x axis labels
        val cal = Calendar.getInstance()

        // loop through each time unit to create the steps by time/date bar graph
        for (stepTime in 0 until stepsArray.size) {

            if (timeUnit == "weeks") {
                minXVal = 0
                maxXVal = stepTime
            } else {
                maxXVal = stepsArray[stepTime][0]
            }

            var xVal = maxXVal - minXVal

            if (timeUnit == "days" || timeUnit == "weeks" || timeUnit == "months") {
                xVal = stepTime
                newXVal = maxXVal
            }

            if (newXVal == 0) {
                newXVal = maxXVal
            }

            // set the y value for the bar
            var stackSteps = 0f
            if (stepTime == stepsArray.size - 1) {
                stackSteps = projectedSteps - stepsArray[stepTime][1].toFloat()
            }

            entries.add(BarEntry(xVal.toFloat(), floatArrayOf(stepsArray[stepTime][1].toFloat(),
                    stackSteps)))

            // set the x label for the bar (need to loop through several bars if zero steps taken in time period)
            while (newXVal <= maxXVal) {
                // depending on the type of time unit, set the tick labels for the x axis
                when (timeUnit) {
                    "hours" -> {
                        xAxisLabels.add(newXVal.toString() + ":00  ")
                        //xAxisLabels.add(stepsArray[stepTime][0].toString() +":00  ")
                    }
                    "days" -> {
                        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                        cal.set(Calendar.YEAR, newXVal.toString().substring(0, 4).toInt())
                        cal.set(Calendar.MONTH, newXVal.toString().substring(4, 6).toInt() - 1)
                        cal.set(Calendar.DAY_OF_MONTH, newXVal.toString().substring(6, 8).toInt())
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                    "weeks" -> {
                        val sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                        cal.set(Calendar.YEAR, stepsArray[stepTime][2])
                        cal.set(Calendar.WEEK_OF_YEAR, stepsArray[stepTime][0])
                        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                        cal.minimalDaysInFirstWeek = 6
                        if (weekStart == 2) {
                            cal.firstDayOfWeek = 1
                            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                        } else {
                            cal.firstDayOfWeek = 0
                            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
                        }
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                    "months" -> {
                        val sdf = SimpleDateFormat("MMMM", Locale.getDefault())
                        cal.set(Calendar.MONTH, newXVal - 1)
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                    "years" -> {
                        val sdf = SimpleDateFormat("yyyy", Locale.getDefault())
                        cal.set(Calendar.YEAR, newXVal)
                        xAxisLabels.add(sdf.format(cal.time))
                    }
                }
                newXVal = newXVal + 1
            }
        }
        // apply the formatted x axis tick labels
        xAxis.valueFormatter = IndexAxisValueFormatter(xAxisLabels)

        // create the graph
        val set = BarDataSet(entries, "Steps")
        set.stackLabels = arrayOf("Steps", "Projected")

        // do not draw labels on the bars
        set.setDrawValues(false)

        // use the main color from the current theme for the bars in the chart
        val theColor = requireContext().getThemeColor(R.attr.colorPrimary)
        val theColor2 = requireContext().getThemeColor(R.attr.colorAccent)
        set.setColors(theColor, theColor2)

        val data = BarData(set)
        fragmentStepSummaryBinding!!.stepsGraph.data = data

        // set the graph colors to match the current theme
        @ColorInt val theChartTextColor = resolveColorAttr(requireView().context,
                android.R.attr.textColorPrimary)
        fragmentStepSummaryBinding!!.stepsGraph.setBorderColor(theChartTextColor)
        fragmentStepSummaryBinding!!.stepsGraph.xAxis.textColor = theChartTextColor
        fragmentStepSummaryBinding!!.stepsGraph.xAxis.gridColor = theChartTextColor
        fragmentStepSummaryBinding!!.stepsGraph.xAxis.axisLineColor = theChartTextColor
        fragmentStepSummaryBinding!!.stepsGraph.axisRight.textColor = theChartTextColor
        fragmentStepSummaryBinding!!.stepsGraph.axisLeft.textColor = theChartTextColor
        fragmentStepSummaryBinding!!.stepsGraph.legend.textColor = (theChartTextColor)

        val maxStepsArray = paseoDBHelper.getMaxSteps(timeUnit, SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
        val (maxSteps, _) = maxStepsArray[0]
        val minSteps = paseoDBHelper.getMinSteps(timeUnit, SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())
        val averageSteps = paseoDBHelper.getAverageSteps(timeUnit, SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date()).toInt())

        // add a horizontal line at the target steps
        val targetLimitLine = LimitLine(targetSteps.toFloat(), "Target")
        targetLimitLine.lineWidth = 1f
        targetLimitLine.enableDashedLine(10f, 10f, 0f)
        targetLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        targetLimitLine.textSize = 10f
        targetLimitLine.lineColor = theChartTextColor
        targetLimitLine.textColor = theChartTextColor

        // add a horizontal line at the maximum steps
        val maxLimitLine = LimitLine(maxSteps.toFloat(), "Max")
        maxLimitLine.lineWidth = 1f
        maxLimitLine.enableDashedLine(10f, 10f, 0f)
        maxLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
        maxLimitLine.textSize = 10f
        maxLimitLine.lineColor = theChartTextColor
        maxLimitLine.textColor = theChartTextColor

        // do not show max, min and average limit lines for hours summary
        if (timeUnit != "hours") {
            // add a horizontal line at the minimum steps
            val minLimitLine = LimitLine(minSteps.toFloat(), "Min")
            minLimitLine.lineWidth = 1f
            minLimitLine.enableDashedLine(10f, 10f, 0f)
            minLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            minLimitLine.textSize = 10f
            minLimitLine.lineColor = theChartTextColor
            minLimitLine.textColor = theChartTextColor

            // add a horizontal line at the minimum steps
            val avgLimitLine = LimitLine(averageSteps.toFloat(), "Avg")
            avgLimitLine.lineWidth = 1f
            avgLimitLine.enableDashedLine(10f, 10f, 0f)
            avgLimitLine.labelPosition = LimitLine.LimitLabelPosition.RIGHT_TOP
            avgLimitLine.textSize = 10f
            avgLimitLine.lineColor = theChartTextColor
            avgLimitLine.textColor = theChartTextColor


            fragmentStepSummaryBinding!!.stepsGraph.axisLeft.removeAllLimitLines() // reset all limit lines to avoid overlapping lines
            fragmentStepSummaryBinding!!.stepsGraph.axisLeft.addLimitLine(maxLimitLine)
            fragmentStepSummaryBinding!!.stepsGraph.axisLeft.addLimitLine(avgLimitLine)
            fragmentStepSummaryBinding!!.stepsGraph.axisLeft.addLimitLine(targetLimitLine)
            fragmentStepSummaryBinding!!.stepsGraph.axisLeft.addLimitLine(minLimitLine)
        }

        // depending on the type of time unit, set the tick labels for the x axis
        when (timeUnit) {
            "hours" -> {
                maxBarsToShow = 24
            }
            "days" -> {
                maxBarsToShow = 10
            }
            "weeks" -> {
                maxBarsToShow = 10
            }
            "months" -> {
                maxBarsToShow = 12
            }
            "years" -> {
                maxBarsToShow = 10
            }
        }

        // set the initial zoom level of the chart
        fragmentStepSummaryBinding!!.stepsGraph.setVisibleXRangeMaximum(maxBarsToShow.toFloat())

        // move the chart to show only the right most bars (user can swipe or pinch to show more bars)
        fragmentStepSummaryBinding!!.stepsGraph.moveViewToX(maxXVal.toFloat())
        fragmentStepSummaryBinding!!.stepsGraph.invalidate() // refresh

        // override the maximum visible range so that the user can pinch to zoom out
        fragmentStepSummaryBinding!!.stepsGraph.setVisibleXRangeMaximum(366f)
        fragmentStepSummaryBinding!!.stepsGraph.minimumHeight = 800
    }
}



//Class MyAdapter
class StepsTableAdapter(private val context: Context, private val arrayList: ArrayList<Array<Int>>,
                        private val timeUnit: String) : BaseAdapter() {
    private lateinit var dateTime: TextView
    private lateinit var steps: TextView



    override fun getCount(): Int {
        return arrayList.size
    }



    override fun getItem(position: Int): Any {
        return position
    }



    override fun getItemId(position: Int): Long {
        return position.toLong()
    }



    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView: View?
        convertView = LayoutInflater.from(context).inflate(R.layout.adapter_step_table_row, parent, false)
        dateTime = convertView.findViewById(R.id.rowDateTime)
        steps = convertView.findViewById(R.id.rowSteps)

        var sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        val cal = Calendar.getInstance()

        // get the application settings
        val paseoPrefs = context.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // default to Monday as first day of week
        val weekStart = paseoPrefs?.getString("prefFirstDayOfWeek", getLocaleFirstDayOfWeek().toString())?.toInt()

        // date to send to the edit steps screen (after long pressing a table row)
        val editDate: Int

        // depending on the type of time unit, format the entry in the first column
        //  (the time/date of the number of steps)
        var theText = ""
        when (timeUnit) {
            "hours" -> {
                theText = arrayList[position][0].toString() + "  "
            }
            "days" -> {
                sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                cal.set(Calendar.DAY_OF_MONTH, arrayList[position][0].toString().substring(6, 8).toInt())
                cal.set(Calendar.MONTH, arrayList[position][0].toString().substring(4, 6).toInt() - 1)
                cal.set(Calendar.YEAR, arrayList[position][0].toString().substring(0, 4).toInt())
            }
            "weeks" -> {
                sdf = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
                cal.set(Calendar.YEAR, arrayList[position][2])
                cal.set(Calendar.WEEK_OF_YEAR, arrayList[position][0])
                cal.minimalDaysInFirstWeek = 6
                if (weekStart == 2) {
                    cal.firstDayOfWeek = 1
                    cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                } else {
                    cal.firstDayOfWeek = 0
                    cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
                }
            }
            "months" -> {
                sdf = SimpleDateFormat("MMMM, yyyy", Locale.getDefault())
                cal.set(Calendar.YEAR, arrayList[position][2])
                cal.set(Calendar.MONTH, arrayList[position][0] - 1)
            }
            "years" -> {
                sdf = SimpleDateFormat("yyyy", Locale.getDefault())
                cal.set(Calendar.YEAR, arrayList[position][0])
            }
        }
        if (timeUnit == "hours") {
            editDate = 0
        }
        else {
            theText = sdf.format(cal.time) + "   "
            sdf = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
            editDate = sdf.format(cal.time).toInt()
        }

        dateTime.text = theText
        steps.text = NumberFormat.getIntegerInstance().format(arrayList[position][1])

        // colour the steps number when the number of steps for that time period has been
        //  previously edited by the user
        if (arrayList[position][3] == 1) {
            @ColorInt val textColor = resolveColorAttr(context,
                    android.R.attr.colorSecondary)

            steps.setTextColor(textColor)
        }

        // put the date in the row's tag field so that when the user chooses to edit the number
        //  of steps for that time/date, the edit screen displays for the selected time/date
        //  rather than the current one
        convertView.tag = editDate

        return convertView
    }
}
